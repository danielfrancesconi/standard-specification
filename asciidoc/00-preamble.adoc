[discrete]
== Disclaimer

This specification is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.

If you find errors or you have proposals for enhancements, do not hesitate to contact us using the online discussion group: https://groups.google.com/forum/#!forum/alpinebits.


[discrete]
== About the AlpineBits Alliance

The "AlpineBits Alliance" is a group of SME operating in the touristic sector working together to innovate and open the data exchange in the alpine tourism, and therefore to optimize the online presence, sales and marketing efforts of the hotels and other accommodations in the alpine territory and also worldwide.

*AlpineBits Alliance*                           +
Via Bolzano 63/A                                +
39057 Frangarto / Appiano s.s.d.v. (BZ) - ITALY +
VAT Reg No: IT02797280217                       +
https://www.alpinebits.org                      +
info@alpinebits.org


[discrete]
== AlpineBits Alliance Members

Altea Software Srl - http://www.altea.it                 +
aries.creative KG - http://www.ariescreative.com         +
ASA OHG - http://www.asaon.com                           +
Brandnamic GmbH - http://www.brandnamic.com              +
Dolomiti.it Srl - http://www.dolomiti.it                 +
GardenaNet snc - http://www.gardena.net                  +
HGV - http://www.hgv.it                                  +
IDM Südtirol - Alto Adige - http://www.idm-suedtirol.com +
Internet Consulting GmbH - http://www.inetcons.it        +
Internet Service GmbH - http://www.internetservice.it    +
LTS - http://www.lts.it                                  +
Marketing Factory GmbH - http://www.marketingfactory.it  +
MM-One Group Srl - http://www.mm-one.com                 +
PCS Hotelsoftware GmbH - http://www.pcs-phoenix.com      +
Peer GmbH - http://www.peer.biz                          +
Rateboard GmbH - http://www.rateboard.info               +
Schneemenschen GmbH - http://www.schneemenschen.de       +
SiMedia GmbH - http://www.simedia.com                    +
trick17.media OHG - http://www.trick17.it                +
XLbit snc - http://www.xlbit.com                         +
Yanovis - http://www.yanovis.com                         +

Authors of this document: +
{AlpineBitsR} repository Contributors - https://gitlab.com/alpinebits/developers-kit/graphs/master


