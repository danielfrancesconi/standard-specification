<ResGlobalInfo>
    <Comments>
        <Comment Name="included services">
            <ListItem ListItem="1" Language="de">Parkplatz</ListItem>
            <ListItem ListItem="2" Language="de">Schwimmbad</ListItem>
            <ListItem ListItem="3" Language="de">Skipass</ListItem>
        </Comment>

        <Comment Name="customer comment">
            <Text>
                Sind Hunde erlaubt?

                Mfg.
                Otto Mustermann.
            </Text>
        </Comment>
    </Comments>

    <CancelPenalties>
        <CancelPenalty>
            <PenaltyDescription>
                <Text>
                Cancellation is handled by hotel.
                Penalty is 50%, if canceled within 3 days before show, 100% otherwise.
                </Text>
            </PenaltyDescription>
        </CancelPenalty>
    </CancelPenalties>

    <HotelReservationIDs>
        <!-- ResID_Type 13 -> Internet Broker -->
        <HotelReservationID ResID_Type="13"
                            ResID_Value="Slogan"
                            ResID_Source="www.example.com"
                            ResID_SourceContext="top banner" />
    </HotelReservationIDs>

    <Profiles>
        <ProfileInfo>
            <!-- ProfileType 4 -> Travel Agent --> 
            <Profile ProfileType="4">
                <CompanyInfo>
                    <CompanyName Code="123" CodeContext="ABC">
                        ACME Travel Agency
                    </CompanyName>
                    <AddressInfo>
                        <AddressLine>Musterstraße 1</AddressLine>
                        <CityName>Flaneid</CityName>
                        <PostalCode>12345</PostalCode>
                        <CountryName Code="IT"/>
                    </AddressInfo>
                    <!-- Code 1 -> Voice -->
                    <TelephoneInfo PhoneTechType="1" PhoneNumber="+391234567890"/>
                    <Email>info@example.com</Email>
                </CompanyInfo>
            </Profile>
        </ProfileInfo>
    </Profiles>

    <BasicPropertyInfo HotelCode="123" HotelName="Frangart Inn"/>
</ResGlobalInfo>
