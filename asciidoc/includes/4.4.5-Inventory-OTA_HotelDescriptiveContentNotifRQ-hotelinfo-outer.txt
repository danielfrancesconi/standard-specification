<OTA_HotelDescriptiveContentNotifRQ 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns="http://www.opentravel.org/OTA/2003/05" 
  xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05
                      OTA_HotelDescriptiveContentNotifRQ.xsd" 
  Version="8.000">

  <HotelDescriptiveContents>
    <HotelDescriptiveContent HotelCode="123" HotelName="Frangart Inn" >

      <!-- ... see file in development kit ... -->

    </HotelDescriptiveContent>
  </HotelDescriptiveContents>

</OTA_HotelDescriptiveContentNotifRQ>
