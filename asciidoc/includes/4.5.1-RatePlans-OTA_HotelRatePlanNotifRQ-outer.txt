<?xml version="1.0" encoding="UTF-8"?>
<OTA_HotelRatePlanNotifRQ xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns="http://www.opentravel.org/OTA/2003/05"
        xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_HotelRatePlanNotifRQ.xsd"
        Version="1.000">

    <RatePlans HotelCode="123" HotelName="Frangart Inn">

        <RatePlan RatePlanNotifType="New" CurrencyCode="EUR" RatePlanCode="Rate1-4-HB">

            <BookingRules>
                <BookingRule Start="2014-03-03" End="2014-04-17">
                    ...
                </BookingRule>
            </BookingRules>

            <Rates>
                <!-- "static" and "date dependent" Rate element described below -->
                <Rate> ... </Rate>
            </Rates>

            <Supplements>
                <!-- "static" and "date dependent" Supplement element described below -->
                <Supplement> ... </Supplement>
            </Supplements>

            <Offers>
                <Offer> ... </Offer>
            </Offers>

            <Description Name="title">
                <!-- ... -->
            </Description>

        </RatePlan>

    </RatePlans>

</OTA_HotelRatePlanNotifRQ>
