<BookingRule Start="2016-03-03" End="2016-04-17">
    <LengthsOfStay>
        <LengthOfStay Time="5" TimeUnit="Day" MinMaxMessageType="SetMinLOS"/>
        <LengthOfStay Time="7" TimeUnit="Day" MinMaxMessageType="SetMaxLOS"/>
    </LengthsOfStay>
</BookingRule>
