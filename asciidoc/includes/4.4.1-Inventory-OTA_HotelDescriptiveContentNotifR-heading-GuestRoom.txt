<GuestRoom Code="DZ" MaxOccupancy="2" MinOccupancy="1" MaxChildOccupancy="1">

  <!-- RoomClassificationCode = "42" means Room, 13 Apartment, see OTA table GRI -->
  <TypeRoom StandardOccupancy="2" RoomClassificationCode="42"/>

  <Amenities>
    <!-- 26 means Crib, see OTA table RMA -->
    <Amenity RoomAmenityCode="26"/>
  </Amenities>

  <MultimediaDescriptions>

    <MultimediaDescription InfoCode="25">
      <!-- ... -->
    </MultimediaDescription>

    <MultimediaDescription InfoCode="1">
      <!-- ... -->
    </MultimediaDescription>

    <MultimediaDescription InfoCode="23">
      <!-- ... -->
    </MultimediaDescription>

  </MultimediaDescriptions>

</GuestRoom>
