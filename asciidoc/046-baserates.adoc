[[anchor-4.6]]
=== 4.6. BaseRates

If the [request_param]`action` parameter is `OTA_HotelRatePlan:BaseRates`, the client sends a pull request to *query* rate plans from the server in order to import data back into a PMS or portal.

[[anchor-4.6.1]]
==== 4.6.1. Client request

The parameter [request_param]`request` contains an `OTA_HotelRatePlanRQ` document.

Nested inside one [xml_element_name]#RatePlan# element the following sub-elements are given in order:

* zero or one [xml_element_name]#DateRange# element with [xml_attribute_name]#Start# and [xml_attribute_name]#End# attributes,

* zero or more [xml_element_name]#RatePlanCandidate# elements with attributes [xml_attribute_name]#RatePlanCode# and [xml_attribute_name]#RatePlanID#,

* *one* [xml_element_name]#HotelRef# element with attributes [xml_attribute_name]#HotelCode# and [xml_attribute_name]#HotelName# - the rules are the same as for room availability notifications (section 4.1.1).

Here is an example:

|===
a[baserates_bkg]|
[source,xml]
----
include::includes/4.6.1-BaseRates-OTA_HotelRatePlanRQ.txt[]
----
v|[small]**`samples/BaseRates-OTA_HotelRatePlanRQ.xml`**
|===
Regarding [xml_element_name]#DateRange# and [xml_element_name]#RatePlanCandidate# six cases need to be distinguished:

1. [xml_element_name]#DateRange# omitted, [xml_element_name]#RatePlanCandidate# present:
+
The server will answer with the matching rate plans.

2. [xml_element_name]#DateRange# omitted, [xml_element_name]#RatePlanCandidate# omitted:
+
The server will answer with all the rate plans it has on record. 
Note that the Rate elements are omitted, the response contains just the [xml_element_name]#RatePlan# elements with the [xml_element_name]#Description#
elements.

3. [xml_element_name]#DateRange# empty, [xml_element_name]#RatePlanCandidate# present:
+
If the server supports deltas, it *must* return the changes to the requested
rate plans since the last request by the same client. If the server does not
support deltas, this is not possible and will trigger a warning response.

4. [xml_element_name]#DateRange# empty, [xml_element_name]#RatePlanCandidate# omitted:
+
This is not possible and will trigger a warning response.

5. [xml_element_name]#DateRange# set, [xml_element_name]#RatePlanCandidate# present:
+
Server responds with all rates matching the given date range and rate plan.

6. [xml_element_name]#DateRange# set, [xml_element_name]#RatePlanCandidate# omitted:
+
The server response contains descriptions of the rate plans having rates in the
given date range. Note that the Rate elements are omitted, the response
contains just the [xml_element_name]#RatePlan# elements with the [xml_element_name]#Description#
elements.

[[anchor-4.6.2]]
==== 4.6.2. Server response

The server will send a response indicating the outcome of the request.
The response is a `OTA_HotelRatePlanRS` document.
Any of the four possible {AlpineBitsR} server response outcomes (success, advisory, warning or error) are allowed.
See <<anchor-A,Appendix A>> for details.

In case of a successful outcome, the [xml_element_name]#Success# element is followed by a [xml_element_name]#RatePlans# element with the information about the hotel.

Here is an example:

|===
a[baserates_bkg]|
[source,xml]
----
include::includes/4.6.2-BaseRates-OTA_HotelRatePlanRS.txt[]
----
v|[small]**`samples/BaseRates-OTA_HotelRatePlanRS.xml`**
|===


