[[anchor-3]]
== 3. Handshaking action

The handshaking action allows client and server to agree on the supported {AlpineBitsR} versions and capabilities.
Any {AlpineBitsR} client or server *must* be able to handle this request.
In the rest of this chapter the word "actor" will be used when referring to either client or server.

It is the *client's responsibility* to ensure the server can handle the actions it intends to perform.
Clients are therefore invited to perform the handshaking before issuing the data exchange actions described in section 4.

Please note that this chapter was completely rewritten in {AlpineBitsR} 2018-10 version, and it introduces breaking changes. This was needed to significantly streamline the handshaking progress.

The following table lists all available handshaking actions.

[options="header",cols="3,^2,3,3"]
|===
| usage +
(since) | mandatory | parameter [request_param]`action` (string) | parameter [request_param]`request` and server response (XML document)

[small]| client sends its supported actions and capabilities
 (since 2018-10) 
| YES 
| ``OTA_Ping:Handshaking``
| OTA_PingRQ +
 OTA_PingRS
|===


[[anchor-3.1]]
=== 3.1. Client request

The parameter [request_param]`request` must contain an `OTA_PingRQ` document.

The [xml_element_name]#EchoData# element **must** contain a ``JSON`` object, build according to the following rules:

* The root element is a JSON array with name ``versions``
* Each element of the array ``versions`` is a JSON object

Each object of the ``versions`` array is built according to the following rules:

* A member with name ``version`` and value corresponding to a valid {AlpineBitsR} version string **must** be present (see the first column of the changelog table, e.g. 2018-10 for the first version of {AlpineBitsR} that supports handshaking)
* A JSON array with name ``actions`` **may** be present

Each element of the ``actions`` array is a JSON object build according to the following rules:

* A member with name ``action`` and value corresponding to one supported [request_param]`action`
* A JSON array with name ``supports``, listing all the supported `capabilities` for the sibling `action`

|===
a[generic_bkg]|
[source,xml]
----
include::includes/3-Handshake-PingRQ.txt[]
----
v| example of Handshake client request, only the EchoData element is shown.
|===

For every succesfully negotiated (see below) [request_param]`action`, the client **must** set the `X-AlpineBits-ClientProtocolVersion` HTTP header according to the sibling `version` when performing the subsequent data exchange with the server.

In order to avoid misintepretations, the {AlpineBitsR} versions below `2018-10` **may** be part of the JSON object, but there **must not** be any ``actions`` array specified for them. The handshaking of the legacy versions **must** happen in subsequent requests following the legacy rules.

[[anchor-3.2]]
=== 3.2. Server Response

The response is an `OTA_PingRS` document.

As mandated by OTA, the content of the [xml_element_name]#EchoData# element **must** be identical to what the client sent.

The server **must** add to the response a [xml_element_name]#Warning# element with [xml_attribute_name]#Type# `11` and [xml_attribute_name]#Status# `ALPINEBITS_HANDSHAKE`. The content of this element **must** be the **intersection** between the client announced ``versions``, ``actions`` and ``capabilities`` and what the server actually supports.


|===
a[generic_bkg]|
[source,xml]
----
include::includes/3-Handshake-PingRS.txt[]
----
v|[small]**example of Handshake server response.**
|===

=== 3.3. pass:normal[List of {AlpineBitsR} supported ``actions`` and ``capabilities``]

* `action_OTA_Ping` +
  handshaking action (support for this action is **mandatory**)

* [freerooms_bkg]`action_OTA_HotelAvailNotif` +
  the actor implements handling room availability notifications (FreeRooms)

* [freerooms_bkg]`OTA_HotelAvailNotif_accept_rooms` +
  for room availability notifications (FreeRooms), the actor accepts booking limits for specific rooms

* [freerooms_bkg]`OTA_HotelAvailNotif_accept_categories` +
  for room availability notifications (FreeRooms), the actor accepts booking limits for categories of rooms

* [freerooms_bkg]`OTA_HotelAvailNotif_accept_deltas` +
  for room availability notifications (FreeRooms), the actor accepts partial information (deltas)

* [freerooms_bkg]`OTA_HotelAvailNotif_accept_BookingThreshold` +
  for room availability notifications (FreeRooms), the actor accepts the number of rooms that are considered free but not bookable 

* [guestrequests_bkg]`action_OTA_Read` +
  the actor implements handling quote requests, booking reservations and cancellations (GuestRequests Pull)

* [guestrequests_bkg]`action_OTA_HotelResNotif_GuestRequests` +
  the actor implements pushing quote requests, booking reservations and cancellations (GuestRequests Push)

* [inventory_bkg]`action_OTA_HotelDescriptiveContentNotif_Inventory` +
  the actor implements handling Inventory/Basic (push)

* [inventory_bkg]`OTA_HotelDescriptiveContentNotif_Inventory_use_rooms` +
  for room category information (Inventory), the actor needs information about specific rooms

* [inventory_bkg]`OTA_HotelDescriptiveContentNotif_Inventory_occupancy_children` +
  for room category information (Inventory), the actor supports applying children rebates also for children below the standard occupation

* [inventory_bkg]`action_OTA_HotelDescriptiveContentNotif_Info` +
  the actor implements handling Inventory/HotelInfo (push)

* [inventory_bkg]`action_OTA_HotelDescriptiveInfo_Inventory` +
  the actor implements handling Inventory/Basic (pull)

* [inventory_bkg]`action_OTA_HotelDescriptiveInfo_Info` +
  the actor implements handling Inventory/HotelInfo (pull)

* [rateplans_bkg]`action_OTA_HotelRatePlanNotif_RatePlans` +
  the actor implements handling prices (RatePlans)

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_ArrivalDOW` +
  for prices (RatePlans), the actor accepts arrival DOW restrictions in booking rules

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_DepartureDOW` +
  for prices (RatePlans), the actor accepts departure DOW restrictions in booking rules

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_RatePlan_BookingRule` +
  for prices (RatePlans), the actor accepts "generic" booking rules

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_RatePlan_RoomType_BookingRule` +
  for prices (RatePlans), the actor accepts "specific" booking rules for the given room types

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_RatePlan_mixed_BookingRule` +
  for prices (RatePlans) and *within the same rate plan*, the actor accepts both "specific" and "generic" booking rules. Both "generic" and "specific" rules capabilities *must* still be announced by the actor.

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_Supplements` +
  for prices (RatePlans), the actor accepts supplements

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_FreeNightsOffers` +
  for prices (RatePlans), the actor accepts free nights offers

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_FamilyOffers` +
  for prices (RatePlans), the actor accepts family offers

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_overlay` +
  for prices (RatePlans), the actor accepts the rate plan notif type value `Overlay`

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_RatePlanJoin` +
  for prices (RatePlans), the actor supports grouping RatePlans with different MealPlanCodes under a single price list

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_OfferRule_BookingOffset` +
  for prices (RatePlans), the actor accepts the OfferRule restrictions MinAdvancedBookingOffset and MaxAdvancedBookingOffset

* [rateplans_bkg]`OTA_HotelRatePlanNotif_accept_OfferRule_DOWLOS` +
  for prices (RatePlans), the actor accepts the OfferRule restrictions ArrivalDaysOfWeek, DepartureDaysOfWeek, SetMinLOS and SetMaxLOS

* [baserates_bkg]`action_OTA_HotelRatePlan_BaseRates` +
  the actor implements handling BaseRates

* [baserates_bkg]`OTA_HotelRatePlan_BaseRates_deltas` +
  the actor supports delta information with BaseRates

{AlpineBitsR} *requires* an actor to support at least all mandatory handshaking actions.

All other capabilities are optional.
It is a *client's responsibility* to check for server capabilities before trying to use them.
A server implementation is free to ignore information that requires a capability it doesn't declare.
A server *must*, however, implement all capabilities it declares.


[[anchor-3.4]]
=== 3.4. Unknown or missing actions

Upon receiving a request with an unknown or missing value for action, the server response is the string: `ERROR:unknown or missing action`.

Please note that the legacy "housekeeping actions" (namely: `getVersion` and `getCapabilities`) have been removed from this version of {AlpineBitsR} and will yield to such a response.

[[anchor-3.5]]
=== 3.5. Implementation tips and best practice

* OTA requires the root element of an XML document to have a version attribute. As regards {AlpineBitsR}, the value of this attribute is irrelevant.

* The handshaking action allows actors that are able to support multiple {AlpineBitsR} versions to negotiate the best combinations of versions and capabilities with a single request. The suggested approach for a client is to list all the {AlpineBitsR} versions and the related ``actions`` / ``capabilities`` it supports in a single request.

* Since the server is performing an **intersection** between the received JSON object and its supported versions and capabilities the response might be an empty JSON object (`{}`), this is an indication that there was a failure in parsing the requests' JSON.
