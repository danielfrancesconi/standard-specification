[[anchor-1]]
== 1. Introduction

This documents describes a standard for exchanging traveling and booking information, called *{AlpineBitsR}*.

{AlpineBitsR} builds upon established standards:

* client-server communication is done through stateless HTTPS (the client POSTs data to the server and gets a response) with basic access authentication <<anchor-footnote1,^1^>> and

* the traveling and booking information are encoded in XML following version 2015A of the OpenTravel Schema <<anchor-footnote3,^3^>>^,^ <<anchor-footnote4,^4^>>^,^ <<anchor-footnote5,^5^>> (from here on called OTA2015A) by the OpenTravel Alliance <<anchor-footnote2,^2^>>.

At the current version of the standard, the scope of {AlpineBitsR} covers exchanging the following types of information:

* room availability (FreeRooms),

* reservation inquiries (GuestRequests),

* room category information (Inventory) and

* prices (RatePlans).

{AlpineBitsR} relies on its underlying transport protocol to take care of security issues. Hence *the use of HTTPS is mandatory*.

